package com.example.pedulisehat_test.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class DateHelper {

    private final static String DATE_FORMAT = "yyyy-MM-dd";
    private final static String DATE_NOTIFICATION_FORMAT = "yyyyMMdd";

    public static String[] getThisWeek() {
        String first;
        String end;

        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.getDefault());

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());

        first = dateFormat.format(calendar.getTime());


        calendar.add(Calendar.DATE, 6);

        end = dateFormat.format(calendar.getTime());


        return new String[]{first, end};
    }

    public static String[] getLastWeek() {
        String first;
        String end;

        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.getDefault());

        Calendar calendar = Calendar.getInstance();
        int date = calendar.get(Calendar.DAY_OF_WEEK) - calendar.getFirstDayOfWeek();
        calendar.add(Calendar.DATE, -date - 7);

        first = dateFormat.format(calendar.getTime());


        calendar.add(Calendar.DATE, 6);

        end = dateFormat.format(calendar.getTime());


        return new String[]{first, end};
    }

    public static String[] getThisMonth() {
        String first;
        String end;

        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.getDefault());

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DATE, 1);

        first = dateFormat.format(calendar.getTime());


        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));

        end = dateFormat.format(calendar.getTime());


        return new String[]{first, end};
    }

    public static String[] getLastMonth() {
        String first;
        String end;

        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.getDefault());

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, -1);
        calendar.set(Calendar.DATE, 1);

        first = dateFormat.format(calendar.getTime());


        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));

        end = dateFormat.format(calendar.getTime());


        return new String[]{first, end};
    }

    public static String[] getThisYear() {
        String first;
        String end;

        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.getDefault());

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, calendar.getActualMinimum(Calendar.MONTH));
        calendar.set(Calendar.DATE, 1);

        first = dateFormat.format(calendar.getTime());

        calendar.set(Calendar.MONTH, calendar.getActualMaximum(Calendar.MONTH));
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));

        end = dateFormat.format(calendar.getTime());


        return new String[]{first, end};
    }


    public static int getNotification() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_NOTIFICATION_FORMAT, Locale.getDefault());
        Calendar calendar = Calendar.getInstance();
        return Integer.parseInt(dateFormat.format(calendar.getTime()));
    }


    public static String getTimeDifference(String pDate) {
        String dateDif = "";
        int diffInDays = 0;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.UK);
        format.setTimeZone(TimeZone.getTimeZone("GMT"));
        Calendar c = Calendar.getInstance();
        String formattedDate = format.format(c.getTime());

        Date d1 = null;
        Date d2 = null;
        try {

            d1 = format.parse(formattedDate);
            d2 = format.parse(pDate);
            long diff = d1.getTime() - d2.getTime();

            diffInDays = (int) (diff / (1000 * 60 * 60 * 24));
            if (diffInDays > 0) {
                if (diffInDays == 1) {
                    dateDif = " kemarin";
                } else {
                    dateDif = diffInDays + " hari yang lalu";
                }
            } else {
                int diffHours = (int) (diff / (60 * 60 * 1000));
                if (diffHours > 0) {
                    if (diffHours == 1) {
                        dateDif = diffHours + " jam yang lalu";
                    } else {
                        dateDif = diffHours + " jam yang lalu";
                    }
                } else {

                    int diffMinutes = (int) ((diff / (60 * 1000) % 60));
                    if (diffMinutes == 1) {
                        dateDif = diffMinutes + " menit yang lalu";
                    } else {
                        dateDif = diffMinutes + " menit yang lalu";
                    }

                }
            }

        } catch (ParseException e) {
            // System.out.println("Err: " + e);
            e.printStackTrace();
        }

        return dateDif;

    }

    public static String changeFormatToBahasa(String yyyyMMddHHmmssformat) {
        String returnDate = yyyyMMddHHmmssformat;

        Date date = null;
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.UK);

        try {
            date = format.parse(yyyyMMddHHmmssformat);
        } catch (ParseException e) {
            date = null;
        }

        if (date != null) {
            SimpleDateFormat formatterDay = new SimpleDateFormat("EEEE");
            SimpleDateFormat formatterDayNumber = new SimpleDateFormat("dd");
            SimpleDateFormat formatterMonth = new SimpleDateFormat("MMMM");
            SimpleDateFormat formatterYearTime = new SimpleDateFormat("yyyy, HH:mm");
            String dateDay = replaceDayToBahasa(formatterDay.format(date));
            String dateNumber = formatterDayNumber.format(date);
            String dateMonth = replaceMonthToBahasa(formatterMonth.format(date));
            String dateYeardkk = formatterYearTime.format(date);
            returnDate = dateDay + ", " + dateNumber + " " + dateMonth + " " + dateYeardkk;
        }

        return returnDate;

    }

    public static String changeFormatToBahasaWithoutTime(String yyyyMMddHHmmssformat) {
        String returnDate = yyyyMMddHHmmssformat;

        Date date = null;
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.UK);

        try {
            date = format.parse(yyyyMMddHHmmssformat);
        } catch (ParseException e) {
            date = null;
        }

        if (date != null) {
            SimpleDateFormat formatterDay = new SimpleDateFormat("EEEE");
            SimpleDateFormat formatterDayNumber = new SimpleDateFormat("dd");
            SimpleDateFormat formatterMonth = new SimpleDateFormat("MMMM");
            SimpleDateFormat formatterYearTime = new SimpleDateFormat("yyyy");
            String dateDay = replaceDayToBahasa(formatterDay.format(date));
            String dateNumber = formatterDayNumber.format(date);
            String dateMonth = replaceMonthToBahasa(formatterMonth.format(date));
            String dateYeardkk = formatterYearTime.format(date);
            returnDate = dateDay + ", " + dateNumber + " " + dateMonth + " " + dateYeardkk;
        }

        return returnDate;

    }

    private static String replaceDayToBahasa(String day) {
        if (day.equalsIgnoreCase("sunday")) {
            return "Minggu";
        } else if (day.equalsIgnoreCase("monday")) {
            return "Senin";
        } else if (day.equalsIgnoreCase("tuesday")) {
            return "Selasa";
        } else if (day.equalsIgnoreCase("wednesday")) {
            return "Rabu";
        } else if (day.equalsIgnoreCase("thursday")) {
            return "Kamis";
        } else if (day.equalsIgnoreCase("friday")) {
            return "Jum'at";
        } else if (day.equalsIgnoreCase("saturday")) {
            return "Sabtu";
        } else {
            return day;
        }

    }


    private static String replaceMonthToBahasa(String month) {
        if (month.equalsIgnoreCase("January")) {
            return "Januari";
        } else if (month.equalsIgnoreCase("February")) {
            return "Februari";
        } else if (month.equalsIgnoreCase("March")) {
            return "Maret";
        } else if (month.equalsIgnoreCase("April")) {
            return "April";
        } else if (month.equalsIgnoreCase("May")) {
            return "Mei";
        } else if (month.equalsIgnoreCase("June")) {
            return "Juni";
        } else if (month.equalsIgnoreCase("July")) {
            return "Juli";
        } else if (month.equalsIgnoreCase("August")) {
            return "Agustus";
        } else if (month.equalsIgnoreCase("September")) {
            return "September";
        } else if (month.equalsIgnoreCase("October")) {
            return "Oktober";
        } else if (month.equalsIgnoreCase("November")) {
            return "November";
        } else if (month.equalsIgnoreCase("December")) {
            return "Desember";
        } else {
            return month;
        }
    }

    public static String replaceMonthCountToBahasa(int month) {
        if (month==1) {
            return "Januari";
        } else if (month==2) {
            return "Februari";
        } else if (month==3) {
            return "Maret";
        } else if (month==4) {
            return "April";
        } else if (month==5) {
            return "Mei";
        } else if (month==6) {
            return "Juni";
        } else if (month==7) {
            return "Juli";
        } else if (month==8) {
            return "Agustus";
        } else if (month==9) {
            return "September";
        } else if (month==10) {
            return "Oktober";
        } else if (month==11) {
            return "November";
        } else if (month==12) {
            return "Desember";
        } else {
            return "Not Found";
        }
    }

    public static String dayLeft(String dayText){
        String text = dayText;
        Calendar calCurr = Calendar.getInstance();
        Calendar day = Calendar.getInstance();
        if(dayText!=null) {
            try {
                day.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(dayText));
                SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
                String inputString2 = myFormat.format(day.getTime());
                String inputString1 = myFormat.format(calCurr.getTime());
                try {
                    Date date1 = myFormat.parse(inputString1);
                    Date date2 = myFormat.parse(inputString2);
                    long diff = date2.getTime() - date1.getTime();
                    text = ("" + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } catch (ParseException e) {

            }
        }
        if(Integer.valueOf(text)>0){
            return text;
        }
        else{
            return "0";
        }

    }

}
