package com.example.pedulisehat_test.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;

public class ScreenSizedView extends FrameLayout {
    public ScreenSizedView(Context context) {
        super(context);
    }

    public ScreenSizedView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ScreenSizedView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ScreenSizedView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setupDimensions();
    }

    private void setupDimensions() {
        Resources resources = getResources();
        if (resources != null) {
            DisplayMetrics metrics = resources.getDisplayMetrics();
            if (metrics != null) {
                int width = metrics.widthPixels;
                int height = metrics.heightPixels;
                setMeasuredDimension(width, height);
            }
        }
    }

    public static void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
        win.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }
}
