package com.example.pedulisehat_test.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import androidx.core.app.NotificationCompat;


public class ReminderBroadcast extends BroadcastReceiver {
    private NotificationHelper notificationHelper;

    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationCompat.Builder nb = notificationHelper.getChannel1Notification("title","body");
        notificationHelper.getManager().notify((int)Math.random()*10, nb.build());
    }
}
