package com.example.pedulisehat_test.model;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ShalatViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public ShalatViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is shalat fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}