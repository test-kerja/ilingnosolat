package com.example.pedulisehat_test.view;

import android.content.Context;
import android.content.ContextWrapper;

import androidx.annotation.VisibleForTesting;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONObject;

import javax.inject.Inject;

import timber.log.Timber;

public class IlingnoAppBase extends MultiDexApplication {


//
//    private Scheduler mScheduler;
//    private ApplicationComponent mApplicationComponent;
    public static JSONObject referringParamsBranchIO;
    public static Context context;

    public static IlingnoAppBase get(Context context) {
        return (IlingnoAppBase) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        new Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(getPackageName())
                .setUseDefaultSharedPreference(true)
                .build();

//        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
//                .setDefaultFontPath("fonts/CentraleSansBook.ttf")
//                .setFontAttrId(R.attr.fontPath)
//                .build()
//        );

//        mApplicationComponent = DaggerApplicationComponent.builder().applicationModule(new ApplicationModule(this)).build();
//        mApplicationComponent.inject(this);
//        Branch.getAutoInstance(this);
        this.context=this;
//        ImagePipelineConfig config = ImagePipelineConfig.newBuilder(this)
//                .setProgressiveJpegConfig(new SimpleProgressiveJpegConfig())
//                .setResizeAndRotateEnabledForNetwork(true)
//                .setDownsampleEnabled(true)
//                .build();
//        Fresco.initialize(this,config);
    }

//    public ApplicationComponent getApplicationComponent() {
//        return mApplicationComponent;
//    }

//    @VisibleForTesting
//    public void setApplicationComponent(ApplicationComponent applicationComponent) {
//        this.mApplicationComponent = applicationComponent;
//    }
//
//    public Scheduler getSubscribeScheduler() {
//        if (mScheduler == null) {
//            mScheduler = Schedulers.io();
//        }
//        return mScheduler;
//    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Timber.e("########## onLowMemory ##########");
    }

    @Override
//    public void onTerminate() {
//        mEventBus.unregister(this);
//        super.onTerminate();
//    }
//
//    @Subscribe
//    public void onEvent(MessagesEvent event) {
//        Timber.e("Unauthorized! Redirect to Signin Activity..!.");
//    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}