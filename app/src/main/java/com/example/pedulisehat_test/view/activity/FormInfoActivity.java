package com.example.pedulisehat_test.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.pedulisehat_test.R;
import com.example.pedulisehat_test.util.KeyHelper;
import com.pixplicity.easyprefs.library.Prefs;

import butterknife.BindView;

public class FormInfoActivity extends BaseActivity{
    @BindView(R.id.button_submit)
    TextView btnSubmit;
    @BindView(R.id.et_username)
    EditText username;

    @Override
    protected int getResourceLayout() {
        return R.layout.page_activity_form_info;
    }

    @Override
    protected void onViewReady(Bundle p0) {
        Intent i = new Intent(this, MainActivity.class);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Prefs.putString(KeyHelper.USERNAME, username.getText().toString());
                startActivity(i);
                finish();
            }
        });
    }
}
