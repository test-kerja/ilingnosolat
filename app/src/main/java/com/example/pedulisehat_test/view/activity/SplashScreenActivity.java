package com.example.pedulisehat_test.view.activity;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.pedulisehat_test.R;
import com.example.pedulisehat_test.util.KeyHelper;
import com.pixplicity.easyprefs.library.Prefs;

public class SplashScreenActivity extends BaseActivity {

    private Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Initialize the Prefs class

    }

    @Override
    protected int getResourceLayout() {
        return R.layout.page_activity_splashscreen;
    }

    @Override
    protected void onViewReady(Bundle p0) {
        Intent i;
        if(Prefs.getString(KeyHelper.USERNAME,"").isEmpty()){
            i = new Intent(this, FormInfoActivity.class);
        }else {
            i = new Intent(this, MainActivity.class);
        }
        Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    startActivity(i);
                    finish();
                }
            }, 2000);

    }

}
