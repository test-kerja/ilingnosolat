package com.example.pedulisehat_test.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pedulisehat_test.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.ItemViewHolder> {

    Integer waktu;
    Context context;
    LayoutInflater inflater;

    public RVAdapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    public void setResource(Integer waktu) {
        this.waktu = waktu;
        notifyDataSetChanged();
    }

    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rv, parent, false);
        return new ItemViewHolder(v);
    }


    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {

        if(position==waktu){
            holder.area.setVisibility(View.VISIBLE);
            holder.area2.setVisibility(View.GONE);
        }else {
            holder.area.setVisibility(View.GONE);
            holder.area2.setVisibility(View.VISIBLE);
        }

        if(position==0){
            holder.textView.setText("Subuh");
            holder.waktu.setText("04:26");
            holder.ic.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_subuh));
            holder.textView2.setText("Subuh");
            holder.waktu2.setText("04:26");
            holder.ic2.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_subuh));
        }else if(position == 1) {
            holder.textView.setText("Terbit");
            holder.waktu.setText("05:43");
            holder.ic.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_terbit));
            holder.textView2.setText("Terbit");
            holder.waktu2.setText("05:43");
            holder.ic2.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_terbit));
        }else if(position == 2) {
            holder.textView.setText("Dzuhur");
            holder.waktu.setText("11:46");
            holder.ic.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_duhur));
            holder.textView2.setText("Dzuhur");
            holder.waktu2.setText("11:46");
            holder.ic2.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_duhur));
        }else if(position == 3) {
            holder.textView.setText("Ashar");
            holder.waktu.setText("14:57");
            holder.ic.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_ashar));
            holder.textView2.setText("Ashar");
            holder.waktu2.setText("14:57");
            holder.ic2.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_ashar));
        }else if(position == 4) {
            holder.textView.setText("Maghrib");
            holder.waktu.setText("17:49");
            holder.ic.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_maghrib));
            holder.textView2.setText("Maghrib");
            holder.waktu2.setText("17:49");
            holder.ic2.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_maghrib));
        }else if(position == 5) {
            holder.textView.setText("Isya");
            holder.waktu.setText("18:58");
            holder.ic.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_isya));
            holder.textView2.setText("Isya");
            holder.waktu2.setText("18:58");
            holder.ic2.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_isya));
        }

    }

    @Override
    public int getItemCount() {
        return 6;
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {



        @BindView(R.id.ic)
        ImageView ic;
        @BindView(R.id.text)
        TextView textView;
        @BindView(R.id.waktu)
        TextView waktu;
        @BindView(R.id.area)
        View area;

        @BindView(R.id.ic2)
        ImageView ic2;
        @BindView(R.id.text2)
        TextView textView2;
        @BindView(R.id.waktu2)
        TextView waktu2;
        @BindView(R.id.area2)
        View area2;

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }
}
