package com.example.pedulisehat_test.view.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;

import com.pixplicity.easyprefs.library.Prefs;

import butterknife.ButterKnife;
import timber.log.Timber;

import static com.example.pedulisehat_test.util.ScreenSizedView.setWindowFlag;

public abstract class BaseActivity extends AppCompatActivity
{
    protected Context mContext;
    protected Toolbar mToolbar;
    protected LayoutInflater mInflater;
    protected ActionBar mActionBar;

    public BaseActivity() {
        this.mContext = (Context)this;
    }

    protected void onCreate(final Bundle savedInstanceState) {

        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }



        super.onCreate(savedInstanceState);
        this.setContentView(this.getResourceLayout());
        ButterKnife.bind((Activity)this);
        Timber.tag(this.getClass().getSimpleName());
        this.mInflater = LayoutInflater.from(this.mContext);
        this.onViewReady(savedInstanceState);

        // Initialize the Prefs class

    }

    public FragmentManager getBaseFragmentManager() {
        return super.getSupportFragmentManager();
    }

    protected void setupToolbar(final Toolbar toolbar) {
        this.setupToolbar(toolbar, null);
    }

    @TargetApi(21)
    protected void setupToolbar(final Toolbar toolbar, final View.OnClickListener onClickListener) {
        this.setSupportActionBar(this.mToolbar = toolbar);
        this.mActionBar = this.getSupportActionBar();
        if (this.mActionBar != null) {
            this.mActionBar.setHomeButtonEnabled(true);
        }
        if (onClickListener != null) {
            toolbar.setNavigationOnClickListener(onClickListener);
        }
    }

    public Toolbar getToolbar() {
        return this.mToolbar;
    }

    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case 16908332: {
                this.onBackPressed();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    public void setTitle(final int title) {
        super.setTitle(title);
        if (this.mActionBar != null) {
            this.mActionBar.setTitle((CharSequence)this.getString(title));
        }
    }

    public ActionBar getBaseActionBar() {
        final ActionBar actionBar = this.getSupportActionBar();
        assert actionBar != null;
        return actionBar;
    }

    public void onBackPressed() {
        if (this.getBaseFragmentManager().getBackStackEntryCount() > 0) {
            this.getBaseFragmentManager().popBackStack();
        }
        else {
            super.onBackPressed();
        }
    }

    protected void showToast(final String message) {
        Toast.makeText(this.mContext, (CharSequence)message, Toast.LENGTH_SHORT).show();
    }

    protected abstract int getResourceLayout();

    protected abstract void onViewReady(final Bundle p0);
}
