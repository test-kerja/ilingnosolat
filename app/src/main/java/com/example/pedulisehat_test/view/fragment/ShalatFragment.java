package com.example.pedulisehat_test.view.fragment;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.pedulisehat_test.R;
import com.example.pedulisehat_test.model.ShalatViewModel;
import com.example.pedulisehat_test.util.DateHelper;
import com.example.pedulisehat_test.util.KeyHelper;
import com.example.pedulisehat_test.util.NotificationHelper;
import com.example.pedulisehat_test.view.adapter.RVAdapter;
import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.pixplicity.easyprefs.library.Prefs;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;

public class ShalatFragment extends AppBaseFragment {
    public static final String inputFormat = "HH:mm";

    Date newDate = new Date();
    SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
    SimpleDateFormat inputParser = new SimpleDateFormat(inputFormat, Locale.US);
    private NotificationHelper notificationHelper;
    long diff;
    long oldLong;
    long NewLong;
    private ShalatViewModel dashboardViewModel;
    @BindView(R.id.text_tanggal)
    TextView textTanggal;
    @BindView(R.id.text_countdown)
    TextView textCountdown;
    @BindView(R.id.text_waktu_solat)
    TextView textWaktuSolat;
    @BindView(R.id.rv_jadwal)
    XRecyclerView rvJadwal;

    String currentTime, subuhTime,subuhTime2, terbitTime, dzuhurTime, asharTime, maghribTime, isyaTime;
    Date oldDate, newwDate;
    SimpleDateFormat formatter;
    boolean subuh, terbit, dzuhur, ashar, maghrib, isya;

    private Date compare,tengahwengi,shubuh,terbits,duhur,asar,magrib,isak;
    private Date dateCompareOne;
    private Date dateCompareTwo;

    RVAdapter adapter;

    @Override
    protected int getResourceLayout() {
        return R.layout.fragment_shalat;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onViewReady(@Nullable Bundle savedInstanceState) {
        dashboardViewModel =
                ViewModelProviders.of(this).get(ShalatViewModel.class);

        notificationHelper = new NotificationHelper(getContext());


        SimpleDateFormat bulan = new SimpleDateFormat("MM");
        bulan.setLenient(false);
        Date date = new Date();

        SimpleDateFormat tahun = new SimpleDateFormat("yyyy");
        tahun.setLenient(false);
        Date dates = new Date();



        adapter = new RVAdapter(getContext());
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        rvJadwal.setLayoutManager(llm);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rvJadwal.setAdapter(adapter);
        rvJadwal.setNestedScrollingEnabled(false);
        rvJadwal.setPullRefreshEnabled(false);
        rvJadwal.setLoadingMoreEnabled(false);

        cekTimer();
        textTanggal.setText(DateHelper.replaceMonthCountToBahasa(Integer.parseInt(bulan.format(date)))+" "+tahun.format(dates));
    }

    public void sendOnChannel1(String tittle, String message){
        NotificationCompat.Builder nb = notificationHelper.getChannel1Notification(tittle, message);
        notificationHelper.getManager().notify((int)Math.random()*10, nb.build());
    }

    public void sendOnChannel2(String tittle, String message){
        NotificationCompat.Builder nb = notificationHelper.getChannel2Notification(tittle, message);
        notificationHelper.getManager().notify(2, nb.build());
    }

    public class MyCount extends CountDownTimer {
        MyCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public void onFinish() {
            cekTimer();
            if(terbit){
                sendOnChannel1("Sudah masuk waktu Terbit "+Prefs.getString(KeyHelper.USERNAME,""),"Shalat dulu yuk");
            }else if(dzuhur){
                sendOnChannel1("Sudah masuk waktu Dzuhur "+Prefs.getString(KeyHelper.USERNAME,""),"Shalat dulu yuk");
            }else if(ashar){
                sendOnChannel1("Sudah masuk waktu Ashar "+Prefs.getString(KeyHelper.USERNAME,""),"Shalat dulu yuk");
            }else if(maghrib){
                sendOnChannel1("Sudah masuk waktu Maghrib "+Prefs.getString(KeyHelper.USERNAME,""),"Shalat dulu yuk");
            }else if(isya){
                sendOnChannel1("Sudah masuk waktu Isya "+Prefs.getString(KeyHelper.USERNAME,""),"Shalat dulu yuk");
            }else if(subuh){
                sendOnChannel1("Sudah masuk waktu Subuh "+Prefs.getString(KeyHelper.USERNAME,""),"Shalat dulu yuk");
            }
        }

        @Override
        public void onTick(long millisUntilFinished) {
            long millis = millisUntilFinished;
            String hms =(TimeUnit.MILLISECONDS.toHours(millis) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millis)) + ":")
                    + (TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)) + ":"
                    + (TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))));
            textCountdown.setText(/*context.getString(R.string.ends_in) + " " +*/ hms);
        }
    }

    @SuppressLint("SimpleDateFormat")
    public static String getTomorrow() {
        Calendar cal = new GregorianCalendar();
        cal.add(Calendar.DATE, 1);
        return new SimpleDateFormat("dd.MM.yyyy").format(cal.getTime());
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void cekTimer(){
        Date currentDate = new Date();
        formatter = new SimpleDateFormat("dd.MM.yyyy, HH:mm:ss");
        currentTime = formatter.format(currentDate);
        subuhTime=getTomorrow()+", 04:26:00";
        subuhTime2=format.format(newDate)+", 04:26:00";
        terbitTime=format.format(newDate)+", 05:43:00";
        dzuhurTime=format.format(newDate)+", 11:46:00";
        asharTime=format.format(newDate)+", 14:57:00";
        maghribTime=format.format(newDate)+", 17:49:00";
        isyaTime=format.format(newDate)+", 18:58:00";

        try {
            compare =new SimpleDateFormat("dd.MM.yyyy, HH:mm:ss").parse(currentTime);
            tengahwengi =new SimpleDateFormat("dd.MM.yyyy, HH:mm:ss").parse(format.format(newDate)+", 00:00:01");
            shubuh =new SimpleDateFormat("dd.MM.yyyy, HH:mm:ss").parse(subuhTime2);
            terbits =new SimpleDateFormat("dd.MM.yyyy, HH:mm:ss").parse(terbitTime);
            duhur =new SimpleDateFormat("dd.MM.yyyy, HH:mm:ss").parse(dzuhurTime);
            asar =new SimpleDateFormat("dd.MM.yyyy, HH:mm:ss").parse(asharTime);
            magrib =new SimpleDateFormat("dd.MM.yyyy, HH:mm:ss").parse(maghribTime);
            isak =new SimpleDateFormat("dd.MM.yyyy, HH:mm:ss").parse(isyaTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if ( compare.after(isak)) {
            subuh();
            Log.e("yoman","1");
            return;
        }
        else if(compare.after(tengahwengi)&&compare.before(shubuh)){
            subuh2();
            Log.e("yoman","2");
            return;
        }else if(compare.before(terbits)){
            terbit();
            return;
        }else if(compare.before(duhur)){
            dzuhur();
            return;
        }else if(compare.before(asar)){
            ashar();
            return;
        }else if(compare.before(magrib)){
            maghrib();
            return;
        }else if(compare.before(isak)){
            isya();
            return;
        }
    }

    public void subuh(){
        isya = false;
        subuh = true;
        try {
            oldDate = formatter.parse(currentTime);
            newwDate = formatter.parse(subuhTime);
            oldLong = oldDate.getTime();
            NewLong = newwDate.getTime();
            diff = NewLong - oldLong;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        MyCount counter = new MyCount(diff, 1000);
        counter.start();
        textWaktuSolat.setText("Subuh 04:26");
        adapter.setResource(0);
    }public void subuh2(){
        isya = false;
        subuh = true;
        try {
            oldDate = formatter.parse(currentTime);
            newwDate = formatter.parse(subuhTime2);
            oldLong = oldDate.getTime();
            NewLong = newwDate.getTime();
            diff = NewLong - oldLong;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        MyCount counter = new MyCount(diff, 1000);
        counter.start();

        textWaktuSolat.setText("Subuh 04:26");
        adapter.setResource(0);
    }public void terbit(){
        subuh = false;
        terbit = true;
        try {
            oldDate = formatter.parse(currentTime);
            newwDate = formatter.parse(terbitTime);
            oldLong = oldDate.getTime();
            NewLong = newwDate.getTime();
            diff = NewLong - oldLong;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        MyCount counter = new MyCount(diff, 1000);
        counter.start();

        textWaktuSolat.setText("Terbit 05:43");

        adapter.setResource(1);
    }public void dzuhur(){
        terbit=false;
        dzuhur=true;
        try {
            oldDate = formatter.parse(currentTime);
            newwDate = formatter.parse(dzuhurTime);
            oldLong = oldDate.getTime();
            NewLong = newwDate.getTime();
            diff = NewLong - oldLong;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        MyCount counter = new MyCount(diff, 1000);
        counter.start();
        adapter.setResource(2);
        textWaktuSolat.setText("Dzuhur 11:46");
    }public void ashar(){
        dzuhur=false;
        ashar=true;
        try {
            oldDate = formatter.parse(currentTime);
            newwDate = formatter.parse(asharTime);
            oldLong = oldDate.getTime();
            NewLong = newwDate.getTime();
            diff = NewLong - oldLong;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        MyCount counter = new MyCount(diff, 1000);
        counter.start();
        adapter.setResource(3);
        textWaktuSolat.setText("Ashar 14:57");
    }public void maghrib(){
        ashar=false;
        maghrib=true;
        try {
            oldDate = formatter.parse(currentTime);
            newwDate = formatter.parse(maghribTime);
            oldLong = oldDate.getTime();
            NewLong = newwDate.getTime();
            diff = NewLong - oldLong;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        MyCount counter = new MyCount(diff, 1000);
        counter.start();
        adapter.setResource(4);
        textWaktuSolat.setText("Maghrib 17:49");
    }public void isya(){
        maghrib=false;
        isya=true;
        try {
            oldDate = formatter.parse(currentTime);
            newwDate = formatter.parse(isyaTime);
            oldLong = oldDate.getTime();
            NewLong = newwDate.getTime();
            diff = NewLong - oldLong;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        MyCount counter = new MyCount(diff, 1000);
        counter.start();
        adapter.setResource(5);
        textWaktuSolat.setText("Isya 18:58");
    }
}
